import React, { Component } from 'react';
import { connect } from "react-redux";
import './homePage.scss'
import {getAllPosts} from "./actionCreators";
import PostList from "./postList/postList";
import ReactPaginate from 'react-paginate';

class HomePage extends Component {

    state = {
        posts:[],
        currentPage: 1,
        postsPerPage: 15,
        firstPost: 0,
        lastPost: 15

    };
    componentDidMount() {
        this.props.dispatch(getAllPosts());
    }
    componentDidUpdate(prevProps, prevState, snapshot) {
        if (this.state.posts.length === 0){
            this.setState({
                posts: this.props.allPosts
            })
        }
    }


    onPageClick = (event) => {
        const page = event.selected+1;

        this.setState({
            currentPage: page,
            firstPost: this.state.postsPerPage * page - this.state.postsPerPage,
            lastPost: this.state.postsPerPage * page,

        })

    };


    render() {

        const {posts, firstPost, lastPost} = this.state;

        let pagination = <ReactPaginate pageCount={10}
                                    pageRangeDisplayed={3}
                                    marginPagesDisplayed={2}
                                    previousLabel={'<'}
                                    nextLabel={'>'}
                                    pageClassName='page-pag'
                                    activeClassName='page-active'
                                    breakClassName='page-pag'
                                    previousClassName='page-pag'
                                    nextClassName='page-pag'
                                    onPageChange={this.onPageClick }

        />;
        if (posts.length !== 0) {
            return(
                <div className='main-container'>
                    <h1 className='page-title centered'>Home sweet home</h1>
                    <PostList allPosts={posts}  current={firstPost} last={lastPost}/>
                    <div className='page-pag-div'>
                        {pagination}
                    </div>

                </div>


        )}else return <div></div>
        }

}
const mapStateToProps = state => {
    return {
        allPosts: state.getAllPostsReducer.posts
    }
};
export default connect(mapStateToProps)(HomePage);
