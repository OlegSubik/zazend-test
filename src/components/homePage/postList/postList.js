import React, {Component} from 'react';

import "./postList.scss"
import {LinkButton} from "../../shared/button/linkButton";

class  PostList extends Component{


    render() {
        const {allPosts, current, last} = this.props;
        allPosts.forEach(post => {
            if (post.completed.toString() === 'true') {
                post.completed = 'Yes'
            } else post.completed = 'No'
        });

        return (
            <ul>

                {allPosts.slice(current, last).map((post, index) => {
                    return (
                        <li key={index} className='post'>
                            <h4 className='post-title'>{post.title.toUpperCase()}</h4>
                            <div className='post-desc'>
                                <p> Completed: {post.completed}</p>
                                <div ><span>User id: {post.userId}</span> <span className='float-right'> Id: {post.id}</span></div>
                                <LinkButton href={`/post/${post.id}`} value={'View post'}/>

                            </div>

                        </li>
                    )
                })}
            </ul>
        )
    }
};


export default PostList;



