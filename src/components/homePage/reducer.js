import {GET_ALL_POSTS} from "../../constants/actionTypes";

const initialState = {
    posts: []
};


export const getAllPostsReducer = (state = initialState, action) =>{

    // тут доцільніше був би if замість switch, але зробив з розрахунком на можливысть розширення

    switch (action.type) {
        case GET_ALL_POSTS:
            return {posts: action.payload};
        default: return state;
    }
};