import axios from 'axios';
import {apiUrl} from "../../config/urls";
import {GET_ALL_POSTS} from "../../constants/actionTypes";

export function getAllPosts() {
    return function (dispatch) {
        return axios.get(apiUrl, )
            .then(res => res.data)
            .then(res => {dispatch({type: GET_ALL_POSTS, payload: res})})
        
    }
    
}