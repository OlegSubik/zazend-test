import React, {Component} from 'react';
import './linkButton.scss'


export class LinkButton extends Component {

    render() {
        const {href, value} = this.props;
        return (
            <div className='centered'><a href={href} className='button view-btn' >{value}</a></div>
        );
    }
}
