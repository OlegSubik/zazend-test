import React, {Component} from 'react';
import {NavLink} from "react-router-dom";
import './header.scss'

class Header extends Component {

    state = {
        icon: true
    };
    toggle = () => {
        document.getElementById('1').classList.toggle('toggle');
        this.setState({
            icon: !this.state.icon
        })
    };
    render() {
        let icon = null;
        if (this.state.icon){
            icon = require('../../assets/icons/Arrow-Down-2-icon.png');
        } else {
            icon = require('../../assets/icons/Arrow-Up-2-icon.png')
        }

        return(
            <header className='header'>
                <a href="/" className='logo'>TestApp</a>
                <img src={icon} className='icon' alt='toggle arrow' onClick={this.toggle}/>
                <ul id={1} className='main-nav toggle'>
                    <li><NavLink to='/' exact activeClassName='active'>Home</NavLink></li>
                    <li><NavLink to='/contacts' exact activeClassName='active' > Contacts</NavLink></li>
                    <li><NavLink to='/about' exact activeClassName='active' >About</NavLink></li>
                </ul>
            </header>


        )
    }
}

export default Header;
