import * as React from 'react';
import './notFoundPage.scss'
import {LinkButton} from "../shared/button/linkButton";


class NotFound extends React.Component {
    render() {
        let gif = require("../../assets/icons/404.gif");
        return (
            <div className='main-container'>
                <h1 className='page-title centered'>Oops! Page not found </h1>
                <LinkButton href={'/'} value={'Go home'}/>
                <img src={gif} alt="404 Not Found Page"/>

            </div>


        );
    }
}
export default NotFound
