import axios from 'axios';
import {apiUrl} from "../../config/urls";
import {GET_SINGLE_POST} from "../../constants/actionTypes";

export function getSinglePost(id) {
    return function (dispatch) {
        return axios.get(apiUrl)
            .then(res => res.data.filter((post) => post.id === id))
            .then(res => {dispatch({type: GET_SINGLE_POST, payload: res})})

    }

}