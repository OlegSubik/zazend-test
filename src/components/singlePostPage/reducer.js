import {GET_SINGLE_POST} from "../../constants/actionTypes";

const initialState = {
    post: []
};


export const getSinglePostReducer = (state = initialState, action) =>{

    // тут доцільніше був би if замість switch, але зробив з розрахунком на можливысть розширення

    switch (action.type) {
        case GET_SINGLE_POST:
            return {post: action.payload[0]};
        default: return state;
    }
};