import React, { Component } from 'react';
import { connect } from "react-redux";
import { getSinglePost} from "./actionCreator";
import './singlePost.scss'

class SinglePostPage extends Component {

       componentDidMount() {
        this.props.dispatch(getSinglePost(Number(this.props.match.params.id)))

    }

    render() {
           let completed = null;
           if (this.props.post.comlpeted && this.props.post.comlpeted.toString === true){
               completed = 'Yes'
           }else {
               completed = 'No'
           }
           if(this.props.post.title){
               return(
                   <div className='main-container'>
                       <h4 className='post-title s-title'>{this.props.post.title.toUpperCase()}</h4>
                       <p className='post-desc s-desc'> Some description of an article. Some description of an article. Some description of an article. Some description of an article. Some description of an article.
                           Some description of an article. Some description of an article. Some description of an article. Some description of an article. Some description of an article.
                           Some description of an article. Some description of an article. Some description of an article. Some description of an article. Some description of an article.
                           Some description of an article. Some description of an article. Some description of an article. Some description of an article. Some description of an article.
                           Some description of an article. Some description of an article. Some description of an article. Some description of an article. Some description of an article.
                           Some description of an article. Some description of an article. Some description of an article. Some description of an article. Some description of an article. </p>
                       <p> Completed: {completed}</p>

                       <div className='s-info' ><span>User id: {this.props.post.userId}</span> <span className='float-right'> Id: {this.props.post.id}</span></div>

                   </div>
               )
           } else {
               return <div></div>
           }



    }
}
const mapStateToProps = state => {
    return {
        post: state.getSinglePostReducer.post
    }
};
export default connect(mapStateToProps)(SinglePostPage);
