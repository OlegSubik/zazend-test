import React from 'react';
import {NavLink} from "react-router-dom";
import './footer.scss'

const Footer = () => {

        return(
            <footer>
                <ul className='foot-nav'>
                    <li><NavLink to='/' exact activeClassName='active'>Home</NavLink></li>
                    <li><NavLink to='/contacts' exact activeClassName='active' > Contacts</NavLink></li>
                    <li><NavLink to='/about' exact activeClassName='active' >About</NavLink></li>
                </ul>
            </footer>

        )
}

export default Footer;
