import { combineReducers } from "redux";
import { getAllPostsReducer} from "../components/homePage/reducer";
import { getSinglePostReducer} from "../components/singlePostPage/reducer";


const rootReducer = combineReducers({
    getAllPostsReducer,
    getSinglePostReducer
});

export default rootReducer;