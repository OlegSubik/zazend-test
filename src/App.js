import React from 'react';

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Provider } from "react-redux";
import { store } from "./store/appStore";
import HomePage from './components/homePage/homePage'
import SinglePostPage from "./components/singlePostPage/singlePostPage";
import ContactsPage from './components/contactPage/contactPage';
import AboutPage from './components/aboutPage/aboutPage';
import NotFoundPage from './components/notFoundPage/notFoundPage';
import Header from "./components/header/header";
import Footer from "./components/footer/footer";

class App extends React.Component {

    render() {
        return (

            <Provider store={store}>
                <Router>
                    <Header/>
                    <div className="App" >

                        <Switch >
                            <Route exact path='/' component={HomePage} />
                            <Route path='/contacts' component={ContactsPage} />
                            <Route path='/post/:id' component={SinglePostPage} />
                            <Route path='/about' component={AboutPage} />
                            <Route component={NotFoundPage} />
                        </Switch>

                    </div>
                    <Footer/>
                </Router>
            </Provider>
        )
    }
}

export default App;

